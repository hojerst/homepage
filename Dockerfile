FROM klakegg/hugo:0.111.3-ext@sha256:2d1841844d7c4017d13c6949f31a8957cc17aacaf5a3d072c64866471d2b3549 AS build

COPY . /app
WORKDIR /app

RUN hugo

FROM nginx:1.27-alpine@sha256:4ff102c5d78d254a6f0da062b3cf39eaf07f01eec0927fd21e219d0af8bc0591

COPY --from=build /app/public /usr/share/nginx/html
